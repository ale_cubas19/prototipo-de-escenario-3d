using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows;

[RequireComponent(typeof(CharacterController))]
[RequireComponent (typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;

    [Header("Animator Parameters")]
    public string motionParam = "motion";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]

    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;

    private Animator animator;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash ) {
        // calculate the input Magnitude 
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        // dash only if character has reached maxSpeed (animator parameter value)
        if (Speed>= Speed - rotationThreshold && dash)
        {
            Speed = 1.5f;
            }
        // Physically move player
        if (Speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);

            Vector3 right = cam.transform.right;
            Vector3 forward = cam.transform.forward;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            // Rotate the character towards desired move direction based on player input and camera position
            Vector3 desiredMoveDirection = forward * vInput + right * hInput;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                                                  Quaternion.LookRotation(desiredMoveDirection),
                                                  rotationSpeed * Time.deltaTime);
        }
        else if (Speed < rotationThreshold)
        {
            // Stop the character
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }
         }
}





